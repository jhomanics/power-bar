﻿using UnityEngine;

public class LookAt : MonoBehaviour {

	public Transform player;
    public float damping = 10f;
    private void Update()
    {
        var lookPos = player.position - transform.position;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
    }
}
