﻿using UnityEngine;

public class RotatePerpetually : MonoBehaviour {

	public enum RotateAxis {x,y,z}
    public RotateAxis rotateAxis;
    public float degreesPerSecond;
    private void Update()
    {
        if (rotateAxis == RotateAxis.x)
            transform.Rotate(degreesPerSecond * Time.deltaTime, 0, 0);

        if (rotateAxis == RotateAxis.y)
            transform.Rotate(0, degreesPerSecond * Time.deltaTime, 0);

        if (rotateAxis == RotateAxis.z)
            transform.Rotate(0, degreesPerSecond * Time.deltaTime, 0);
    }
}
