﻿using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    #region ENUMS
    public enum Variables
    {
        LoopTotalTime, LoopElapsed, LoopTimeLeft, LoopNormalized, LoopReverseNormalized,
        TotalLoopIterations, CurrentLoopIteration, TimerElapsed, TimerTimeLeft, TimerTotalTime, TimerNormalized, TimerReverseNormalized
    }
    #endregion

    #region HELPERS
    public float GetProperty(Timer.Variables variableEnum)
    {
        switch (variableEnum)
        {
            case Timer.Variables.LoopTotalTime:
                return LoopTotalTime;
            case Timer.Variables.LoopElapsed:
                return LoopElapsed;
            case Timer.Variables.LoopTimeLeft:
                return LoopTimeLeft;
            case Timer.Variables.LoopNormalized:
                return LoopNormalized;
            case Timer.Variables.LoopReverseNormalized:
                return LoopReverseNormalized;
            case Timer.Variables.TotalLoopIterations:
                return TotalLoopIterations;
            case Timer.Variables.CurrentLoopIteration:
                return CurrentLoopIteration;
            case Timer.Variables.TimerElapsed:
                return TotalTimeElapsed;
            case Timer.Variables.TimerTimeLeft:
                return TotalTimeLeft;
            case Timer.Variables.TimerNormalized:
                return TotalNormalized;
            case Timer.Variables.TimerReverseNormalized:
                return TotalReverseNormalized;
            default: return 0;
        }
    }

    [ContextMenu("Refresh")]
    public void Refresh()
    {
        CurrentLoopIteration = 0;
        LoopElapsed = 0;
        IsExpired = false;
    }
    #endregion

    #region LOOP PROPERTIES
    public float LoopTotalTime = 5f;
    public float LoopElapsed
    {
        get { return _loopElapsed; }
        set 
        {
            if (TotalLoopIterations == CurrentLoopIteration)
                _loopElapsed = LoopTotalTime;
            else
            _loopElapsed = Mathf.Clamp(value, 0, LoopTotalTime);
        }
    }
    [SerializeField] private float _loopElapsed = 0f;

    public float LoopTimeLeft { get { return LoopTotalTime - LoopElapsed; } }
    public float LoopNormalized { get { return LoopElapsed / LoopTotalTime; } }
    public float LoopReverseNormalized { get { return LoopTimeLeft / LoopTotalTime; } }
    #endregion

    #region TIMER PROPERTIES

    public bool IsExpired { get { return _isExpired; } private set { if (_isExpired == value) return; _isExpired = value; if (_isExpired) Expired?.Invoke(); } }
    private bool _isExpired;

    public float TotalTimeElapsed
    {
        get 
        {
            float  formula = CurrentLoopIteration * LoopTotalTime + LoopElapsed;
            
            if (TotalLoopIterations != -1)
                formula = Mathf.Clamp(formula, 0, TotalLoopIterations * LoopTotalTime);
            else
                formula = Mathf.Clamp(formula, 0, CurrentLoopIteration * LoopTotalTime);

            _totalTimeElapsed = formula;
            return _totalTimeElapsed;
        }
    }
    [SerializeField] private float _totalTimeElapsed;

    public int TotalLoopIterations
    { 
        get 
        {
            return _totalLoopIterations;
        }
        set 
        {
            _totalLoopIterations = Mathf.Clamp(value, -1, int.MaxValue);
            if (TotalLoopIterations != -1)
                _currentLoopIteration = Mathf.Clamp(_currentLoopIteration, 0, TotalLoopIterations);
        }
    }
    [SerializeField] private int _totalLoopIterations = 1;

    public int CurrentLoopIteration
    {
        get { return _currentLoopIteration; }
        set
        {
            if (_currentLoopIteration == value)
                return;

            
            if (CurrentLoopIteration == TotalLoopIterations)
                LoopElapsed = LoopTotalTime;
            if (TotalLoopIterations == -1)
            _currentLoopIteration = Mathf.Clamp(value, 0, int.MaxValue);
            else
            _currentLoopIteration = Mathf.Clamp(value, 0, TotalLoopIterations);

            if (_currentLoopIteration < TotalLoopIterations) {LoopElapsed = 0;}
            else if (_currentLoopIteration == TotalLoopIterations) { IsExpired = true; }

            Looped?.Invoke();

        }
    }
    [SerializeField] private int _currentLoopIteration = 0;

    public float TotalTimeLeft { get { return TotalTimeToComplete - TotalTimeElapsed; } }
    public float TotalNormalized { get { return TotalTimeElapsed / TotalTimeToComplete; } }
    public float TotalReverseNormalized { get { return TotalTimeLeft / TotalTimeToComplete; } }
    public float TotalTimeToComplete { get { if (TotalLoopIterations == -1) return Mathf.Infinity; else return LoopTotalTime * TotalLoopIterations; } }
    #endregion

    #region EVENTS
    public UnityEvent Looped;
    public UnityEvent Updated;
    public UnityEvent Expired;
    #endregion

    #region UNITY METHODS
    void Update()
    {
        if (IsExpired) return;

        LoopElapsed += Time.deltaTime;
        Updated?.Invoke();

        if (LoopElapsed >= LoopTotalTime)
        {
            CurrentLoopIteration++;
            
        }
    }

    private void LateUpdate()
    {
        if (IsExpired) return;

        if (LoopElapsed >= LoopTotalTime)
            LoopElapsed = 0;
    }
    #endregion
}
