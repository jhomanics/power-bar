﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Timer))]
public class TimerEditor : Editor
{
    public static bool displayTimerSettings = true;
    public static bool displayLoopSettings = true;

    public Timer timer;
    private void OnEnable() 
    {
        timer = target as Timer;
        
        updateEventProperty = serializedObject.FindProperty("Updated");
        loopEventProperty = serializedObject.FindProperty("Looped");
        expireEventProperty = serializedObject.FindProperty("Expired");
    }
    private void OnDisable() { timer = null; }

    public SerializedProperty loopEventProperty;
    public SerializedProperty updateEventProperty;
    public SerializedProperty expireEventProperty;

    public void DisplayLoopSettings()
    {
        EditorGUI.indentLevel++;
            timer.LoopTotalTime = Mathf.Clamp(EditorGUILayout.FloatField("Total Time", timer.LoopTotalTime), 0, float.MaxValue);
            timer.LoopElapsed = EditorGUILayout.FloatField("Elapsed", timer.LoopElapsed);
            EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.FloatField("Time Left", timer.LoopTimeLeft);
                EditorGUILayout.FloatField("Normalized", timer.LoopNormalized);
                EditorGUILayout.FloatField("Reverse Normalized", timer.LoopReverseNormalized);
        EditorGUI.EndDisabledGroup();
        EditorGUI.indentLevel--;
    }

    public void DisplayTimerSettings()
    {
        EditorGUI.indentLevel++;
            timer.TotalLoopIterations = EditorGUILayout.IntField("Total Loop Iterations", timer.TotalLoopIterations);
            timer.CurrentLoopIteration = EditorGUILayout.IntField("Current Loop Iteration", timer.CurrentLoopIteration);
        
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.FloatField("Elapsed", timer.TotalTimeElapsed);
            EditorGUILayout.FloatField("Time Left", timer.TotalTimeLeft);
            EditorGUILayout.FloatField("Total Time", timer.TotalTimeToComplete);
            EditorGUILayout.FloatField("Normalized", timer.TotalNormalized);
            EditorGUILayout.FloatField("Reverse Normalized", timer.TotalReverseNormalized);
            EditorGUI.EndDisabledGroup();
        EditorGUI.indentLevel--;

    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        
        displayLoopSettings = GUILayout.Toggle(displayLoopSettings, "Loop", CustomEditorStyles.CustomFoldoutStyle);
        if (displayLoopSettings) DisplayLoopSettings();

        displayTimerSettings = GUILayout.Toggle(displayTimerSettings, "Timer", CustomEditorStyles.CustomFoldoutStyle);
        if (displayTimerSettings) DisplayTimerSettings();

        EditorGUILayout.PropertyField(updateEventProperty);
        EditorGUILayout.PropertyField(loopEventProperty);
        EditorGUILayout.PropertyField(expireEventProperty);

        serializedObject.ApplyModifiedProperties();
        EditorUtilities.EndChangeCheck();
        
        Repaint();
    }

}
