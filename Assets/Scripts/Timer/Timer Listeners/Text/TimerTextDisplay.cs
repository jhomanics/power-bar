﻿using System;
using TMPro;

public class TimerTextDisplay : TimerListener
{
    public TMP_Text Text;
    public bool displayAsTimeFormat = false;
    
    private void Start(){ Action(); }

    public override void Action()
    {
        float value = timer.GetProperty(timerPropertyToDisplay);

        if (displayAsTimeFormat)
        {
            TimeSpan ts = TimeSpan.FromSeconds(value);
            if (Utilities.IsLessThanAMinute(ts)) Text.text = string.Format("{0:D2}", ts.Seconds);
            else if (Utilities.IsLessThanAnHour(ts)) Text.text = string.Format("{0:D2}:{1:D2}", ts.Minutes, ts.Seconds);
            else if (Utilities.IsLessThanADay(ts)) Text.text = string.Format("{0:D2}:{1:D2}:{2:D2}", ts.Hours, ts.Minutes, ts.Seconds);
            else Text.text = string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D2}", ts.Days, ts.Hours, ts.Minutes, ts.Seconds);
        }
        else
            Text.text = value.ToString();
    }
}
