﻿using UnityEngine.UI;

public class TimerFillImage : TimerListener
{
    public Image image;

    private void Start() { Action(); }
    public override void Action()
    {
        float value = timer.GetProperty(timerPropertyToDisplay);

        image.fillAmount = value;
    }
}
