﻿using UnityEngine;

public abstract class TimerListener : MonoBehaviour
{
    public Timer timer;
    public Timer.Variables timerPropertyToDisplay;
    public abstract void Action();
}
