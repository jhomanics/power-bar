﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;

public static class Utilities
{
#if !UNITY_IOS
    public static I CreateInstanc<I>() where I : class
    {
        string assemblyPath = Environment.CurrentDirectory + "\\DynamicCreateInstanceofclass.exe";

        Assembly assembly;

        assembly = Assembly.LoadFrom(assemblyPath);
        Type type = assembly.GetType("DynamicCreateInstanceofclass.UserDetails");
        return Activator.CreateInstance(type) as I;
    }
#endif

    public static uint Combine(List<uint> list)
    {
        uint combinedValue = 0;
        for (int x = 0; x < list.Count; x++) combinedValue |= list[x];
        return combinedValue;
    }

    public static int Combine(List<int> list)
    {
        int combinedValue = 0;
        for (int x = 0; x < list.Count; x++) combinedValue |= list[x];
        return combinedValue;
    }

    public static uint Combine(uint val1, uint val2)
    {
        return val1 | val2;
    }

    public static int Combine(int val1, int val2)
    {
        return val1 | val2;
    }

    public static DirectoryInfo[] ValidateDirectory(DirectoryInfo root)
    {
        DirectoryInfo[] dirs = null;
        try { dirs = root.GetDirectories(); }
        catch (UnauthorizedAccessException e)
        { Debug.LogError(e.Message); }
        catch (DirectoryNotFoundException e)
        { Debug.LogError(e.Message); }

        return dirs;
    }

    public static DirectoryInfo FindDirectory(string nameOfDirectoryToFind, DirectoryInfo baseRelativeDirectory)
    {
        var subDirs = ValidateDirectory(baseRelativeDirectory);
        foreach (DirectoryInfo subDir in subDirs)
        {
            if (nameOfDirectoryToFind == subDir.Name)
                return subDir;

            var directory = FindDirectory(nameOfDirectoryToFind, subDir);
            if (directory != null)
            {
                if (directory.Name == nameOfDirectoryToFind)
                    return directory;
            }
        }
        return null;
    }

    public static IEnumerator DestroyCR(GameObject go)
    {
        yield return new WaitForEndOfFrame();
        UnityEngine.Object.DestroyImmediate(go);
    }
    public static IEnumerator DestroyCR(Component comp)
    {
        yield return new WaitForEndOfFrame();
        UnityEngine.Object.DestroyImmediate(comp);
    }

    public static GameObject FindParentWithName(GameObject childObject, string name)
    {
        Transform t = childObject.transform;
        while (t != null)
        {
            if (t.gameObject.name == name)
                return t.parent.gameObject;

            if (t.parent == null)
                return null;

            t = t.parent.transform;
        }
        return null; // Could not find a parent with given name.
    }

    public static GameObject FindParentWithTag(GameObject childObject, string tag)
    {
        Transform t = childObject.transform;
        while (t != null)
        {
            if (t.gameObject.tag == tag)
                return t.parent.gameObject;

            if (t.parent == null)
                return null;

            t = t.parent.transform;
        }
        return null; // Could not find a parent with given tag.
    }

    public static Mesh GetUnityPrimitiveMesh(PrimitiveType primitiveType)
    {
        switch (primitiveType)
        {
            case PrimitiveType.Sphere:
                return GetCachedPrimitiveMesh(ref _unitySphereMesh, primitiveType);
            case PrimitiveType.Capsule:
                return GetCachedPrimitiveMesh(ref _unityCapsuleMesh, primitiveType);
            case PrimitiveType.Cylinder:
                return GetCachedPrimitiveMesh(ref _unityCylinderMesh, primitiveType);
            case PrimitiveType.Cube:
                return GetCachedPrimitiveMesh(ref _unityCubeMesh, primitiveType);
            case PrimitiveType.Plane:
                return GetCachedPrimitiveMesh(ref _unityPlaneMesh, primitiveType);
            case PrimitiveType.Quad:
                return GetCachedPrimitiveMesh(ref _unityQuadMesh, primitiveType);
        }
        return null;
    }

    public static Material GetDefaultMaterial()
    {
        return Resources.GetBuiltinResource<MeshRenderer>(GetPrimitiveMeshPath(PrimitiveType.Cube)).material;
    }

    private static Mesh GetCachedPrimitiveMesh(ref Mesh primMesh, PrimitiveType primitiveType)
    {
        if (primMesh == null)
        {
            Debug.Log("Getting Unity Primitive Mesh: " + primitiveType);
            primMesh = Resources.GetBuiltinResource<Mesh>(GetPrimitiveMeshPath(primitiveType));


            if (primMesh == null)
            {
                Debug.LogError("Couldn't load Unity Primitive Mesh: " + primitiveType);
            }
        }

        return primMesh;
    }

    private static string GetPrimitiveMeshPath(PrimitiveType primitiveType)
    {
        switch (primitiveType)
        {
            case PrimitiveType.Sphere:
                return "New-Sphere.fbx";
            case PrimitiveType.Capsule:
                return "New-Capsule.fbx";
            case PrimitiveType.Cylinder:
                return "New-Cylinder.fbx";
            case PrimitiveType.Cube:
                return "Cube.fbx";
            case PrimitiveType.Plane:
                return "New-Plane.fbx";
            case PrimitiveType.Quad:
                return "Quad.fbx";
        }
        return null;
    }

    private static Mesh _unityCapsuleMesh = null;
    private static Mesh _unityCubeMesh = null;
    private static Mesh _unityCylinderMesh = null;
    private static Mesh _unityPlaneMesh = null;
    private static Mesh _unitySphereMesh = null;
    private static Mesh _unityQuadMesh = null;

    public static bool IsLessThanAMinute(TimeSpan ts)
    {
        return ts.Minutes < 1 && ts.Hours < 1 && ts.Days < 1;
    }

    public static bool IsLessThanAnHour(TimeSpan ts)
    {
        return ts.Hours < 1 && ts.Days < 1;
    }

    public static bool IsLessThanADay(TimeSpan ts)
    {
        return ts.Days < 1;
    }
}