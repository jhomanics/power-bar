﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public static class ExtensionMethods
{
    public static void Fade(this CanvasGroup CanvasGroup, float target, float duration)
    {
        Timing.KillCoroutines(CanvasGroup.ToString());
        Timing.RunCoroutine(_Fade(CanvasGroup, target, duration).CancelWith(CanvasGroup.gameObject), CanvasGroup.ToString());
    }

    public static IEnumerator<float> _Fade(this CanvasGroup CanvasGroup, float target, float duration)
    {
        if (target > CanvasGroup.alpha)
        {
            while (CanvasGroup.alpha < target)
            {
                CanvasGroup.alpha += 1 * Time.deltaTime / duration;
                yield return Timing.WaitForOneFrame;
            }
        }
        else if (target < CanvasGroup.alpha)
        {
            while (CanvasGroup.alpha > target)
            {
                CanvasGroup.alpha -= 1 * Time.deltaTime / duration;
                yield return Timing.WaitForOneFrame;
            }
        }
    }

    public static void Fade(this CanvasGroup CanvasGroup, float target, float duration, Action beforeAction, Action afterAction)
    {
        Timing.KillCoroutines(CanvasGroup.ToString());
        Timing.RunCoroutine(_Fade(CanvasGroup, target, duration, beforeAction, afterAction).CancelWith(CanvasGroup.gameObject), CanvasGroup.ToString());
    }

    public static IEnumerator<float> _Fade(this CanvasGroup CanvasGroup, float target, float duration, Action beforeAction, Action afterAction)
    {
        beforeAction?.Invoke();
        if (target > CanvasGroup.alpha)
        {
            while (CanvasGroup.alpha < target)
            {
                CanvasGroup.alpha += 1 * Time.deltaTime / duration;
                yield return Timing.WaitForOneFrame;
            }
        }
        else if (target < CanvasGroup.alpha)
        {
            while (CanvasGroup.alpha > target)
            {
                CanvasGroup.alpha -= 1 * Time.deltaTime / duration;
                yield return Timing.WaitForOneFrame;
            }
        }
        afterAction?.Invoke();
    }
}