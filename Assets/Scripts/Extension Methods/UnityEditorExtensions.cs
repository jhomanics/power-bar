﻿using UnityEditor;

public static class EditorGUILayoutExtended
{
    public static Range RangeField(Range range)
    {
        range.min = EditorGUILayout.FloatField("Min", range.min);
        range.max = EditorGUILayout.FloatField("Max", range.max);

        return range;
    }
}
