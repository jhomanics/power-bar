﻿using UnityEditor;
using System.IO;
using UnityGuidRegenerator;

[InitializeOnLoad]
public static class PackageImporter
{
    public static string GitFilesPath { get { return Path.Combine(EditorUtilities.ProjectPath, "Assets\\README"); } }

    static string gitignoreProjectPath { get { return Path.Combine(EditorUtilities.ProjectPath, ".gitignore"); } }
    static string gitattributesProjectPath { get { return Path.Combine(EditorUtilities.ProjectPath, ".gitattributes"); } }

    static PackageImporter()
    {
        DirectoryInfo gitfilesDir = new DirectoryInfo(GitFilesPath);

        if (!Directory.Exists(gitfilesDir.FullName)) return;

        FileInfo[] files = gitfilesDir.GetFiles();
        for (int x = 0; x < files.Length; x++)
        {
            if (files[x].Name == "gitignore.txt")
                File.Move(Path.Combine(GitFilesPath, files[x].Name), Path.Combine(GitFilesPath, ".gitignore"));
            if (files[x].Name == "gitattributes.txt") { 
                File.Move(Path.Combine(GitFilesPath, files[x].Name), Path.Combine(GitFilesPath, ".gitattributes"));
                UnityGuidRegeneratorMenu.RegenerateGuids();
            }
        }
    }
}
