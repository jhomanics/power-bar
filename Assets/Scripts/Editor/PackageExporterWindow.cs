﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityGuidRegenerator;
using System.IO;

public class PackageExporterWindow : EditorWindow
{
    public static string DefaultPackageName { get { return Application.productName + "-" + Application.version; } }
    public static string DefaultOutputPathName { get { return Path.Combine(EditorUtilities.ProjectPath + "\\built-packages"); } }
    
    private static string TempPackageName { get; set; }
    private static string TempOutputPathName { get; set; }

    private static bool error { get; set; }
    private static string errorMessage { get; set; }

    public static string GitFilesPath { get { return Path.Combine(Application.dataPath, "README"); } }

    static string gitignoreProjectPath { get { return Path.Combine(EditorUtilities.ProjectPath, ".gitignore"); } }
    static string gitattributesProjectPath { get { return Path.Combine(EditorUtilities.ProjectPath, ".gitattributes"); } }

    static FileInfo gitignoreFile { get { return new FileInfo(Path.Combine(EditorUtilities.ProjectPath, ".gitignore")); } }
    static FileInfo gitattributesFile { get { return new FileInfo(Path.Combine(EditorUtilities.ProjectPath, ".gitattributes")); } }


    [MenuItem("Tools/Package Exporter")]
    static void Init()
    {
        TempPackageName = DefaultPackageName;
        TempOutputPathName = DefaultOutputPathName;

        error = false;

        PackageExporterWindow window = (PackageExporterWindow)EditorWindow.GetWindow(typeof(PackageExporterWindow));
        window.Show();
    }

    private void OnGUI()
    {
        Repaint();
        TempPackageName = EditorGUILayout.TextField("Package Name", TempPackageName);

        EditorGUILayout.BeginHorizontal();

            TempOutputPathName = EditorGUILayout.TextField("Output Path (exclude package name)", TempOutputPathName);
            if (GUILayout.Button("Browse")){ TempOutputPathName = EditorUtility.OpenFolderPanel("Path", EditorUtilities.ProjectPath, ""); GUI.FocusControl(null); }
        
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Export"))
        {
            error = false; 
            DirectoryInfo outputDir = new DirectoryInfo(TempOutputPathName);
            if (!outputDir.Exists) { error = true; errorMessage = "The output directory does not exist!"; }

            if (!error)
            {
                if (!Directory.Exists(GitFilesPath)) Directory.CreateDirectory(GitFilesPath);
                    
                MoveGitIgnoreFile();
                MoveGitAttributesFile();

                File.Create(Path.Combine(GitFilesPath, "README.txt")).Dispose();
                File.WriteAllText(Path.Combine(GitFilesPath,"README.txt"), "There are .gitignore and .gitattributes files in this folder. Move them to the main Project folder to ensure proper Git usage with Unity! You may delete the enclosing folder after you have moved them!");
                AssetDatabase.Refresh();

                AssetDatabase.ExportPackage("Assets", Path.Combine(TempOutputPathName, TempPackageName + ".unitypackage"), ExportPackageOptions.Recurse | ExportPackageOptions.IncludeDependencies);

                if (Directory.Exists(GitFilesPath)){ Directory.Delete(GitFilesPath, true); }

                AssetDatabase.Refresh();

                PackageExporterWindow window = (PackageExporterWindow)EditorWindow.GetWindow(typeof(PackageExporterWindow));
                window.Close();

                EditorSceneManager.OpenScene("Assets/Scenes/Main.unity");
            }
        }

        if (error) EditorGUILayout.LabelField(errorMessage, CustomEditorStyles.RedWarning);
    }

    private static void MoveGitIgnoreFile()
    {
        File.Copy(gitignoreProjectPath, Path.Combine(GitFilesPath, gitignoreFile.Name));
        File.Move(Path.Combine(GitFilesPath, gitignoreFile.Name), Path.Combine(GitFilesPath, "gitignore.txt"));
    }

    private static void MoveGitAttributesFile()
    {
        File.Copy(gitattributesProjectPath, Path.Combine(GitFilesPath, gitattributesFile.Name));
        File.Move(Path.Combine(GitFilesPath, gitattributesFile.Name), Path.Combine(GitFilesPath, "gitattributes.txt"));
    }
}