﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;
using System;

public static class EditorUtilities
{
    public static string ProjectPath { get {return Path.GetFullPath(Path.Combine(Application.dataPath, @"..")); } }





    public static bool EndChangeCheck()
    {
        if (Application.isPlaying)
            return false;
     
        var check = EditorGUI.EndChangeCheck();
        if (check)
        {
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            return true;
        }
        else
            return false;
    }

    public static void IndentIncreaseByAmount(int amount)
    {
        if (amount <= 0) return;
        
        EditorGUI.indentLevel++;
        amount--;
        IndentIncreaseByAmount(amount);
    }

    public static void IndentDecreaseByAmount(int amount)
    {
        if (amount <= 0) return;

        EditorGUI.indentLevel--;
        amount--;
        IndentDecreaseByAmount(amount);
    }

    public static void AddSpaces(int amount)
    {
        if (amount <= 0) return;

        EditorGUILayout.Space();
        amount--;
        IndentIncreaseByAmount(amount);
    }

    public static string[] supportedVideoFileTypes = { ".asf", ".avi", ".dv", ".m4v", ".mov", ".mp4", ".mpg", ".mpeg", ".ogv", ".vp8", ".webm", ".wmv" };
    public static bool IsSupportedVideoFile(FileInfo file)
    {
        //Video Type is undefined at this time
        bool flag = false;
        foreach (string videoFileType in EditorUtilities.supportedVideoFileTypes)
        {
            if (file.Extension == videoFileType)
            {
                //Flag set to true meaning that this is indeed a video file!
                flag = true;
                break;
            }
        }

        //If video return true
        if (flag) return true;
        //If not video return false
        if (!flag) return false;

        //If for some reason you reach here....Then it is definitely not a video
        return false;
    }

    public static DirectoryInfo GetStreamingAssetsPath()
    {
        var streamingAssetsPath = " ";

#if UNITY_ANDROID
            streamingAssetsPath = "jar:file://" + Application.dataPath + "!/assets/";
#endif
#if UNITY_IOS
            streamingAssetsPath = Application.dataPath + "/Raw";
#endif
#if UNITY_STANDALONE
        streamingAssetsPath = Application.dataPath + "/StreamingAssets";
#endif

        DirectoryInfo dir = new DirectoryInfo(streamingAssetsPath);
        return dir;
    }

    public static DirectoryInfo FindDirectory(string directoryToFind, DirectoryInfo currentDir)
    {
        DirectoryInfo[] subDirs = null;
        subDirs = ValidateDirectory(subDirs, currentDir);
        DirectoryInfo dirInfo = null;
        foreach (DirectoryInfo dir in subDirs)
        {
            if (directoryToFind == dir.Name)
            {
                dirInfo = dir;
                return dirInfo;
            }
            else
            {
                dirInfo = FindDirectory(directoryToFind, dir);
                if (dirInfo.Name == directoryToFind)
                    return dirInfo;
            }
        }

        return currentDir;
    }

        public static DirectoryInfo[] GetAllDirectories(DirectoryInfo root)
    {
        DirectoryInfo[] subDirs = null;
        subDirs = ValidateDirectory(subDirs, root);
        return subDirs;
    }

    public static DirectoryInfo[] ValidateDirectory(DirectoryInfo[] dirs, DirectoryInfo root)
    {
        try { dirs=  root.GetDirectories(); }
        catch (UnauthorizedAccessException e) { Debug.LogError(e.Message); }
        catch (DirectoryNotFoundException e) { Debug.LogWarning(e.Message); }

        return dirs;
    }

    public static FileInfo[] ValidateFiles(FileInfo[] files, DirectoryInfo root)
    {
        try { files = root.GetFiles("*.*"); }
        catch (UnauthorizedAccessException e) { Debug.LogError(e.Message); }
        catch (DirectoryNotFoundException e) { Debug.LogWarning(e.Message); }

        return files;
    }

    public static bool IsMetaFile(FileInfo file)
    {
        if (file.Name.Contains(".meta")) return true;
        else return false;
    }

    public static bool IsMetaFile(string fileName)
    {
        if (fileName.Contains(".meta")) return true;
        else return false;
    }

}