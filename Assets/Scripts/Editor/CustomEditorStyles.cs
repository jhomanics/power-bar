﻿using UnityEditor;
using UnityEngine;

public static class CustomEditorStyles {
    
    public static GUIStyle Bold
    {
        get 
        { 
            GUIStyle style = new GUIStyle();
            style.fontStyle = FontStyle.Bold;
            style.normal.textColor = GUI.skin.label.normal.textColor;
            return style;
        }
    }

    public static GUIStyle Centered
    {
        get
        { 
            GUIStyle style = new GUIStyle();
            style.normal.textColor = GUI.skin.label.normal.textColor;
            style.alignment = TextAnchor.MiddleCenter;
            return style;
        }
    }

    public static GUIStyle BoldAndCentered
    {
        get
        { 
            GUIStyle style = new GUIStyle();
            style.fontStyle = FontStyle.Bold;
            style.alignment = TextAnchor.MiddleCenter;
            style.normal.textColor = GUI.skin.label.normal.textColor;
            return style;
        }
    }

    public static GUIStyle DefaultGUIStyle
    {
        get
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = GUI.skin.label.normal.textColor;
            return style;
        }
    }

    public static GUIStyle GreenAlert
    {
        get
        { 
            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.green;
            style.fontStyle = FontStyle.Bold;
            style.alignment = TextAnchor.MiddleCenter;
            return style;
        }
    }

    public static GUIStyle RedWarning
    {
        get 
        { 
            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.red;
            style.fontStyle = FontStyle.Bold;
            style.alignment = TextAnchor.MiddleCenter;
            return style;
        }
    }

    public static GUIStyle CustomFoldoutStyle
    {
        get 
        { 
            GUIStyle style = EditorStyles.foldout;
            style.fontStyle = FontStyle.Bold;
            style.alignment = TextAnchor.LowerLeft;
            return style;
        }
    }

    public static GUIStyle CustomFoldoutStyleEnlarged
    {
        get
        {
            GUIStyle style = new GUIStyle(EditorStyles.foldout);
            style.fontStyle = FontStyle.Bold;
            style.alignment = TextAnchor.LowerLeft;
            style.fontSize = 14;
            return style;
        }
    }

    public static GUIStyle SegmentButton
    {
        get
        {
            GUIStyle style = new GUIStyle(GUI.skin.button);
            style.active.textColor = Color.green;
            style.normal.textColor = Color.green;
            return style;
        }
    }

    public static GUIStyle SegmentComponentButton
    {
        get
        {
            GUIStyle style = new GUIStyle(GUI.skin.button);
            style.fixedWidth = 200;
            style.alignment = TextAnchor.MiddleCenter;
            return style;
        }
    }

}
