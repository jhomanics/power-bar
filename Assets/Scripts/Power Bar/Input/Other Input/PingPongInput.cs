﻿using UnityEngine;

public class PingPongInput : PowerBarInput
{
    public float Speed = 1f;
    private void Update()
    {
       PowerBar.Data.BaseCurrentPower = Mathf.Lerp(PowerBar.Data.Range.min, PowerBar.Data.Range.max, Mathf.PingPong(Time.time * Speed, 1));
    }
}
