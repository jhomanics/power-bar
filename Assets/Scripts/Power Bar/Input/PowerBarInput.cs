﻿using UnityEngine;

[RequireComponent(typeof(PowerBar))]
public class PowerBarInput : MonoBehaviour, IPowerBarInput
{
    public PowerBar PowerBar { get; private set; }

    protected virtual void Awake()
    {
        PowerBar = GetComponent<PowerBar>();
    }
}
