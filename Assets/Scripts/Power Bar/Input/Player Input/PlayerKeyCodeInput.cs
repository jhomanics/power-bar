﻿using System;
using UnityEngine;

public class PlayerKeyCodeInput : BasePlayerInput
{
    public float accelerationSpeed = 1f;
    public float deccelerationSpeed = 1f;

    public enum ActionType { GetKey, GetKeyDown, GetKeyUp }
    public ActionType actionType { get { return _actionType; } set { _actionType = value; InputActionChanged?.Invoke(); } }
    [SerializeField] private ActionType _actionType;

    public KeyCode[] keycodes;
    public event Action InputActionChanged;

    public delegate bool ActionMethod(KeyCode keycode);

    public ActionMethod actionMethod;

    private void UpdateActionMethod()
    {
        switch (actionType)
        {
            case ActionType.GetKey:
                actionMethod = Input.GetKey;
                break;
            case ActionType.GetKeyDown:
                actionMethod = Input.GetKeyDown;
                break;
            case ActionType.GetKeyUp:
                actionMethod = Input.GetKeyUp;
                break;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        UpdateActionMethod();
    }

    private void OnEnable()
    {
        InputActionChanged += UpdateActionMethod;
    }

    private void OnDisable()
    {
        InputActionChanged -= UpdateActionMethod;
    }

    private void Update()
    {
        bool keyHit = false;
        for (int x = 0; x < keycodes.Length; x++)
        {
            if (actionMethod(keycodes[x]))
                keyHit = true;
        }

        if (keyHit)
            PowerBar.Data.BaseCurrentPower += accelerationSpeed;
        else
            PowerBar.Data.BaseCurrentPower -= deccelerationSpeed;
    }
}