﻿using System;
using UnityEngine;

[Serializable]
public class PowerBarData
{
    public event Action Updated;

    public Range Range { get { return range; } set { range = value; } }
    [SerializeField] private Range range;

    public float BaseCurrentPower { get { return baseCurrentPower; } set { if (baseCurrentPower == value) return; value = Mathf.Clamp(value, range.min, range.max); baseCurrentPower = value; Updated?.Invoke(); } }
    [SerializeField] private float baseCurrentPower;

#if UNITY_EDITOR
    public bool showInInspector = true;
#endif
}
