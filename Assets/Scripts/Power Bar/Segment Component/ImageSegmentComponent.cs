﻿using UnityEngine.UI;

[System.Serializable]
public class ImageSegmentComponent : BaseSegmentComponent
{
    public Image image;
}
