﻿using UnityEngine;

[System.Serializable]
public class ColorSegmentComponent : BaseSegmentComponent
{ 
   public Color color;
}