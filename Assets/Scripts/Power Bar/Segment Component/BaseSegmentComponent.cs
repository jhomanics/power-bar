﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public abstract class BaseSegmentComponent : ISegmentComponent
{
    public static List<string> TypesNames = new List<string>();
    public static List<ISegmentComponent> Types = new List<ISegmentComponent>();

#if UNITY_EDITOR
    public bool showInInspector = true;
#endif

    static BaseSegmentComponent()
    { 
        var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes())
            .Where(x => typeof(ISegmentComponent).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
            .Select(x => x).ToList();

        foreach (var type in types)
        {
            Types.Add((ISegmentComponent)Activator.CreateInstance(type));
            TypesNames.Add(Types.Last().GetType().ToString());
        }
    }

    public static ISegmentComponent Create(string name)
    {
        foreach (var type in Types)
        {
            if (!type.GetType().Name.Equals(name)) continue;

            return (ISegmentComponent)Activator.CreateInstance(type.GetType());
        }

        return null;
    }
}