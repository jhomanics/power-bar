﻿using System.Collections.Generic;

[System.Serializable]
public class Segment 
{
    public bool IsActive = true;
    public Range range;

    public PowerBar PowerBar { get; set; }

    public List<ISegmentComponent> components = new List<ISegmentComponent>();

    #if UNITY_EDITOR
    public bool showInInspector = true; 
    public bool isAddingComponent = false;
#endif

    public virtual void Enable()
    {
        PowerBar.Data.Updated += OnUpdated;
    }

    public virtual void Disable()
    {
        PowerBar.Data.Updated -= OnUpdated;
    }

    protected virtual void OnUpdated()
    {
        if (PowerBar.Data.BaseCurrentPower < range.min || PowerBar.Data.BaseCurrentPower > range.max)
            IsActive = false;
        else
            IsActive = true;
    }
}
