﻿using UnityEngine;
using OdinSerializer;

public class PowerBar : SerializedMonoBehaviour
{
    public PowerBarData Data { get { return data; } set { data = value; } }
    [SerializeField] private PowerBarData data;
}