﻿using UnityEngine;
using UnityEditor;
using Malee.Editor;

[CustomEditor(typeof(SegmentedPowerBar))]
public class SegmentedPowerBarEditor : Editor
{
    private SegmentedPowerBar powerBar;

    private void OnEnable()
    {
        powerBar = (SegmentedPowerBar)target;
    }

    private int popupIndex = 0;

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
    
        DisplayPowerBarData(powerBar.Data);

        EditorGUILayout.Space();

        for (int x = 0; x < powerBar.segments.Count; x++)
        {
            DisplaySegment(powerBar.segments[x]);
            EditorGUILayout.Space();
        }

        if (GUILayout.Button("Add Segment", CustomEditorStyles.SegmentButton))
            powerBar.segments.Add(new Segment());

        EditorUtilities.EndChangeCheck();
    }

    public void DisplayPowerBarData(PowerBarData data)
    {
        data.showInInspector = EditorGUILayout.Foldout(data.showInInspector, "Data", true);
        if (!data.showInInspector) return;

        EditorGUILayout.LabelField("Range", CustomEditorStyles.Bold);

        EditorGUI.indentLevel++;
            data.Range = EditorGUILayoutExtended.RangeField(data.Range);
        EditorGUI.indentLevel--;

        data.BaseCurrentPower = EditorGUILayout.FloatField("Base Current Power", data.BaseCurrentPower);
    }

    private void DisplaySegment(Segment segment)
    {
        EditorGUILayout.BeginHorizontal();
        segment.showInInspector = EditorGUILayout.Foldout(segment.showInInspector, "Segment", true, CustomEditorStyles.CustomFoldoutStyleEnlarged);
        if (GUILayout.Button("Remove"))
            powerBar.segments.Remove(segment);
        EditorGUILayout.EndHorizontal();
        
        if (!segment.showInInspector) return;

        segment.IsActive = EditorGUILayout.Toggle("Is Active", segment.IsActive);

        EditorGUILayout.LabelField("Range", CustomEditorStyles.Bold);
        EditorGUI.indentLevel++;
        segment.range = EditorGUILayoutExtended.RangeField(segment.range);
        EditorGUI.indentLevel--;

        EditorGUILayout.Space();
       
        if (segment.components.Count > 0) 
            EditorGUILayout.LabelField("Components", CustomEditorStyles.BoldAndCentered);

        for (int a = 0; a < segment.components.Count; a++)
        {
            EditorGUILayout.BeginHorizontal();

            DisplayBaseSegmentComponent(segment.components[a]);

            if (GUILayout.Button("Remove"))
                segment.components.Remove(segment.components[a]);
            
            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Add Segment Component"))
            segment.isAddingComponent = true;

        if (segment.isAddingComponent)
        {
            popupIndex = EditorGUILayout.Popup(popupIndex, BaseSegmentComponent.TypesNames.ToArray());
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Add"))
            {
                segment.components.Add(BaseSegmentComponent.Create(BaseSegmentComponent.TypesNames[popupIndex]));
                segment.isAddingComponent = false;
            }
            if (GUILayout.Button("Cancel"))
                segment.isAddingComponent = false;
            EditorGUILayout.EndHorizontal();
        }

    }

    private void DisplayBaseSegmentComponent(ISegmentComponent component)
    {
        if (component is ColorSegmentComponent)
        {
            var comp = component as ColorSegmentComponent;
            comp.color = EditorGUILayout.ColorField("Color", comp.color);
        }

        if (component is MultiplierSegmentComponent)
        {
            var comp = component as MultiplierSegmentComponent;
            comp.multiplier = EditorGUILayout.FloatField("Multiplier", comp.multiplier);
        }
    }
}   
