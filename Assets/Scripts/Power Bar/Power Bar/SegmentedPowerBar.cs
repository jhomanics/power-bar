﻿using System.Collections.ObjectModel;

public class SegmentedPowerBar : PowerBar
{
    public ObservableCollection<Segment> segments = new ObservableCollection<Segment>();

    protected virtual void Awake()
    {
        for (int x = 0; x < segments.Count; x++)
            segments[x].PowerBar = this;
    }

    protected virtual void OnEnable()
    {
        for (int x = 0; x < segments.Count; x++)
            segments[x].Enable();
    }

    protected virtual void OnDisable()
    {
        for (int x = 0; x < segments.Count; x++)
            segments[x].Disable();
    }
}
