﻿using UnityEngine;
using UnityEngine.UI;

public class PowerBarUI : MonoBehaviour
{
    public PowerBar powerBar;

    public Slider Slider { get; private set; }

    protected virtual void Awake() { Slider = GetComponent<Slider>(); }

    protected virtual void Start() { UpdateSliderValues(); }

    protected virtual void OnEnable() { powerBar.Data.Updated += OnUpdated; }

    protected virtual void OnDisable() { powerBar.Data.Updated -= OnUpdated; }

    private void OnUpdated()
    {
        UpdateSliderValues();
    }

    private void UpdateSliderValues()
    {
        Slider.minValue = powerBar.Data.Range.min;
        Slider.maxValue = powerBar.Data.Range.max;
        Slider.value = powerBar.Data.BaseCurrentPower;
    }
}