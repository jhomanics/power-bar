﻿using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.UI;

public class SegmentedPowerBarUI : PowerBarUI
{
    public GameObject SegmentContainerPrefab;
    public GameObject SegmentPrefab;

    public GameObject SegmentContainer { get; private set; }
    protected List<GameObject> SegmentsUI = new List<GameObject>();

    protected override void Start() { base.Start(); UpdateSegments(); }

    protected override void OnEnable() { base.OnEnable(); var pb = powerBar as SegmentedPowerBar; pb.segments.CollectionChanged += OnCollectionChanged; }

    protected override void OnDisable() { base.OnDisable(); var pb = powerBar as SegmentedPowerBar; pb.segments.CollectionChanged -= OnCollectionChanged; }

    private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
        UpdateSegments();
    }

    protected virtual void UpdateSegments()
    {
        if (SegmentContainer)
            Destroy(SegmentContainer);

        SegmentContainer = Instantiate(SegmentContainerPrefab, this.transform);
        SegmentContainer.transform.SetAsFirstSibling();

        SegmentsUI = new List<GameObject>();

        List<float> segmentWeights = new List<float>();
        
        var powerBar = this.powerBar as SegmentedPowerBar;

        for (int x = 0; x < powerBar.segments.Count; x++)
        {
            var segment = Instantiate(SegmentPrefab, SegmentContainer.transform);
            segment.GetComponent<LayoutElement>().preferredWidth = (Mathf.Abs(powerBar.segments[x].range.min - powerBar.segments[x].range.max));
            segmentWeights.Add(segment.GetComponent<LayoutElement>().preferredWidth);
            
            for (int y = 0; y < powerBar.segments[x].components.Count; y++)
            {
                if (powerBar.segments[x].components[y] is ColorSegmentComponent)
                    CreateColorComponent(segment, powerBar.segments[x].components[y]);
            }
            
            SegmentsUI.Add(segment);
        }

        DetermineSegmentLayout(segmentWeights);
        
    }

    private void DetermineSegmentLayout(List<float> segmentWeights)
    {
        segmentWeights.Sort((a, b) => a.CompareTo(b));

        for (int y = 0; y < segmentWeights.Count; y++)
        {
            for (int x = 0; x < SegmentsUI.Count; x++)
            {
                if (segmentWeights[y] == SegmentsUI[x].GetComponent<LayoutElement>().preferredWidth)
                {
                    SegmentsUI[x].GetComponent<LayoutElement>().flexibleWidth = y + 1;
                    SegmentsUI[x].GetComponent<LayoutElement>().flexibleWidth = Mathf.Clamp(SegmentsUI[x].GetComponent<LayoutElement>().flexibleWidth, 1, float.PositiveInfinity);
                }
            }
        }
    }

    private void CreateColorComponent(GameObject segment, ISegmentComponent component)
    {
        var colorComponent = component as ColorSegmentComponent;
        segment.AddComponent<Image>();
        segment.GetComponent<Image>().color = colorComponent.color;
    }

}
